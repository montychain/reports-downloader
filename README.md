# Reports Extractor

#### Installation
Script needs the following Python3 modules to be installed:  
**requests**  
**python-docx**  


#### Edit Google Docs report links (if needed)
Edit em at **settings.py** in a list called **FILE_URLS**


#### Running script
Run **main.py**
```bash
python3 main.py
```
Script will create a new folder named **report_day.month.year** containing all the downloaded reports


#### Today/Tomorrow placeholder replacement
You can add placeholders for today or tomorrow anywhere in the Google Docs file
```
{today}
{tomorrow}
```
and script will replace them as today's or tomorrow's actual date

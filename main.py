from settings import *


def download_single_file(doc_url, dir_name):
    doc_id = FIND_ID.findall(doc_url)[0]
    dl_url = ROOT_DL_URL + doc_id
    r = requests.get(dl_url, headers=HEADERS)
    assert r.status_code == 200, f"Got non-200 status code ({r.status_code}) for requesting {doc_url}"
    file = r.content
    cd = requests.utils.unquote(r.headers["content-disposition"])
    file_name = cgi.parse_header(cd)[-1]["filename*"]
    file_name = file_name.replace("UTF-8\'\'", "")
    file_name = datetime.today().strftime('%d_%m_%Y') + "_" + file_name
    file_path = os.path.join(dir_name, file_name)
    with open(file_path, "wb") as f:
        f.write(file)
    update_dates(file_path)


def update_dates(file_path):
    document = Document(file_path)
    for i, p in enumerate(document.paragraphs):
        t = p.text
        if "{today}" in t:
            p.text = t.replace("{today}", TODAY)
        if "{tomorrow}" in t:
            p.text = t.replace("{tomorrow}", TOMORROW)
    document.save(file_path)


def download_all_files():
    dir_name = "reports_" + TODAY
    if os.path.exists(dir_name):
        print("Folder named", dir_name, "exists. Deleting it and making a new one!")
        shutil.rmtree(dir_name)
    os.mkdir(dir_name)
    for url in FILE_URLS:
        download_single_file(url, dir_name)


if __name__ == '__main__':
    download_all_files()

import requests
import shutil
import cgi
import os
import re
from docx import *
from datetime import datetime, timedelta

today = datetime.today()
tomorrow = today + timedelta([1, 1, 1, 1, 3, 2, 1][today.weekday()])
TODAY = today.strftime('%d.%m.%Y')
TOMORROW = tomorrow.strftime('%d.%m.%Y')
# date_today + [1, 1, 1, 1, 3, 2, 1][date_today.weekday()]
ROOT_DL_URL = "https://docs.google.com/document/u/0/export?format=docx&id="
FILE_URLS = [
    # "https://docs.google.com/document/d/1HcNWLzHNeQnjUuNc-A8RI0CmD_NVRhuAV-Byt51K04Q/edit?usp=sharing",  # Акимов. Отправляет Романову
    "https://docs.google.com/document/d/1sNUy8LIK9Eu8cjRaQvRwdSZvlaSukkSxp9RJ832i6GA/edit?usp=sharing",
    "https://docs.google.com/document/d/1QHdmyV60AmJVPmNiix-wvHBTx1-DQoZaOLPyldTIDRU/edit?usp=sharing",
    "https://docs.google.com/document/d/1JclDZPsGFQ1kCeQF8SbDUJGCKbxvap7F_uHK6GQoN1Q/edit?usp=sharing",
    "https://docs.google.com/document/d/1l0qU1j7Gp4wAzt0X2_iGIefMXq091jxkrrUDBpFO-FI/edit",
    "https://docs.google.com/document/d/11UyQnxhUo86oFz1wQKCyU7CQOJxFxuIW0TmvECatQko/edit",
    "https://docs.google.com/document/d/1dh0q93s6s0TgNMv0f2iG25InUstD1w7wyZKV-KRtwf8/edit?usp=sharing",
    # "https://docs.google.com/document/d/1PEeBVn7k9ghncK_fEluNbBa_IpBj4RgPUnjm1MQ9fFs/edit"  # Холодов. Уволился.
]

HEADERS = {
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) "
                  "Chrome/79.0.3945.88 Safari/537.36",
}

FIND_ID = re.compile(r'document/d/([\w_-]*)')
FIND_FILE_NAME = re.compile(f'UTF-8\'\'([\w%.]+)\.docx')
